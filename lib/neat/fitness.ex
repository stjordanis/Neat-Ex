defmodule Neat.Fitness do
  require Neat
  alias Neat.Utils

  @doc "Given Neat, each member of the population is replaced by a tuple including its fitness: {ann, fitness}."
  def map_fitness(neat = %Neat{opts: opts, species: species}) do
    species = if opts.single_fitness_function do
      Utils.async_species_map species, fn {rep, members} ->
        {rep, members_map_fitness(members, opts)}
      end
    else
      opts.fitness_function.(neat)
    end
    Map.put(neat, :species, species)
  end

  def members_map_fitness(members, opts) do
    Enum.map(members, fn
      ({ann, fitness}) -> {ann, fitness}
      (ann) -> {ann, opts.fitness_function.(ann)}
    end)
  end

  @doc "Given Neat, each species representative is replaced by the tuple {ann, average_fitness_of_species, {time_since_last_improvement, max_fitness}}. time_since_last_improvement counts the number of time steps since the max_fitness has gone up."
  def assign_avg_fitness(neat = %Neat{species: species}) do
    species = Enum.map species, fn {{rep, _prevAvg, {tsi, oldMax}}, members} ->
      fitnesses = Enum.map(members, fn {_ann, fitness} -> fitness end)
      max = Enum.max(fitnesses)
      avg = Enum.sum(fitnesses) / length(members)
      {
        {rep, avg, {(if max == oldMax, do: tsi + 1, else: 0), max}},
        members
      }
    end
    Map.put(neat, :species, species)
  end
end
