defmodule Examples.Xor do
  @moduledoc "Code for evolving neural networks to reproduce the behavior of the XOR logic gates (only with 0's repalced by -1's)."
  alias Ann.Simulation

  def run(verbose \\ false, gens \\ -1) do
    neat = Neat.new_single_fitness(
      Ann.new([1, 2, 3], [4]),
      &xor_fitness_function/1
    )
    print = fn neat ->
      {ann, fitness} = neat.best
      if verbose, do: IO.puts("Gen: #{neat.generation}, fitness: #{fitness}, species num: #{length(neat.species)}, comp_thrsh: #{neat.opts.compatibility_threshold}"), else: IO.write "."
      sim = Simulation.new(ann)
      Enum.each dataset(), fn {{in1, in2}, out} ->
        result = Map.get(Simulation.eval(sim, %{1=>in1, 2=>in2, 3=>1.0}).data, 4, 0)
        if verbose, do: IO.puts "#{in1} xor #{in2} = #{result}, error: #{abs(1 - result)*0.1 + :math.pow(abs(result - out), 1.1)}"
      end
    end
    {time, neat} = :timer.tc fn ->
      if gens == -1, do: Neat.evolveUntil(neat, 63, print), else: Neat.evolveFor(neat, gens, print)
    end
    {ann, fitness} = neat.best

    if verbose do
      Ann.saveGZ(ann, "xor.gz")
      IO.puts "\nSolved!!! In only #{neat.generation} generations, #{Float.round(time / 1000000.0, 2)} seconds, with fitness #{fitness}."
    else
      IO.write "!"
    end
    neat
  end

  def xor_fitness_function({_ann, fitness}), do: fitness
  def xor_fitness_function(ann) do
    sim = Ann.Simulation.new(ann)
    error = Enum.reduce dataset(), 0, fn {{in1, in2}, out}, error ->
      result = Map.get(Ann.Simulation.eval(sim, %{1=>in1, 2=>in2, 3=>1.0}).data, 4, 0)
      error + abs(result - out)
    end
    :math.pow(8 - error, 2)
  end

  def dataset do
    [{{-1, -1}, -1}, {{1, -1}, 1}, {{-1, 1}, 1}, {{1, 1}, -1}]
  end
end
