defmodule NeuralTrainerTest do
  use ExUnit.Case

  # doctest GradAprox.NeuralTrainer

  @moduletag timeout: 600_000_000

  setup do
    ann = Ann.newFeedforward([1, 2], [3], [2])
    {:ok, [
      ann: Map.put(ann, :connections,
        ann.connections
          |> Map.put(100, Ann.Connection.new(3, 6))
          |> Map.put(101, Ann.Connection.new(3, 7))
          |> Map.put(102, Ann.Connection.new(6, 6))
      ),
    ]}
  end

  test "XOR NeuralTraining", context do
    {ann, data} = GradAprox.NeuralTrainer.maximize context[:ann], fn ann ->
      {fitness, _acceptable} = xor_fitness_function(ann)
      fitness
    end, %{learn_val: 0.001, delta: 0.05, terminate?: fn ann, _info ->
      {_fitness, acceptable} = xor_fitness_function(ann, 4)
      acceptable
    end}
    Ann.saveGZ(ann, "gradAproxXOR.gz")
    IO.write "(XOR NeuralTraining: #{data.step})"
  end

  def xor_fitness_function(ann, number \\ 1) do
    sim = Ann.Simulation.new(ann)
    {error, acceptable} = Enum.reduce Enum.take_random(dataset(), number), {0, true}, fn {{in1, in2}, out}, {error, acceptable} ->
      result = Map.get(Ann.Simulation.eval(sim, %{1 => in1, 2 => in2}).data, 3, 0)
      {error + abs(result - out), acceptable && Float.round(result) == out}
    end
    fitness = :math.pow(8 - error, 2)
    {min(fitness, 60), acceptable}
  end

  def dataset do
    [{{-1, -1}, -1}, {{1, -1}, 1}, {{-1, 1}, 1}, {{1, 1}, -1}]
  end
end
