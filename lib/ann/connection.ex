defmodule Ann.Connection do
  @moduledoc "A module for storing connections between nodes in an ANN."
  defstruct input: -1, output: -1, weight: 0.0, enabled: true

  alias Ann.Connection

  @doc false
  def new do
    %Connection{}
  end

  @doc "Creates a new Ann.Conn."
  def new(input, output, weight \\ 0.1*(2*:rand.uniform - 1), enabled \\ true) do
    %Connection{input: input, output: output, weight: weight, enabled: enabled}
  end

  @doc "Deprecated. Cantor's pairing function. Given positive integers (a, b), a unique number is returned such that no other integers can produce the same number."
  def genCantorId(a, b) do
    trunc(0.5 * (a+b) * (a+b+1)  + b)
  end

  @doc "Convinience function for creating a list of Ann.Conns from a list of `{input, output, weight}`, `{id, input, output, weight}`, or `{id, input, output, weight, enabled}` tuples."
  def createFromList(list) do
    {conns, _} = Enum.reduce list, {%{}, 0}, fn
      {input, output, weight}, {acc, id} ->
        {Map.put(acc, id, Connection.new(input, output, weight)), id+1}
      {given_id, input, output, weight}, {acc, id} ->
        {Map.put(acc, given_id, Connection.new(input, output, weight)), max(id, given_id+1)}
      {given_id, input, output, weight, enabled}, {acc, id} ->
        {Map.put(acc, given_id, Connection.new(input, output, weight, enabled)), max(id, given_id+1)}
    end
    conns
  end
end
