defmodule AnnTest do
  use ExUnit.Case

  doctest Ann

  setup do
    ann = Ann.new([1, 2, 3], [4], Ann.Connection.createFromList(
        [#{in, out, weight},
          {1, 5, 0.25},
          {2, 5, 0.75},
          {3, 5, -0.4},
          {5, 4, 0.25},
          {1, 4, 0.1},
          {5, 5, -0.2}
        ]
      )
    )
    {:ok, [ann: ann]}
  end

  test "Ann Serialization", context do
    ann = context[:ann]
    json = Ann.json(ann)
    ann2 = Ann.new(json)
    assert ann == ann2, "Issue with ANN (de)serialization.\nSerialized to #{json}\nand got #{inspect ann2}\nbut expected #{inspect ann}"
  end
end
